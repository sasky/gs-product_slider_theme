<?php
/*
Plugin Name: Google Splash, a theme for the WPEC Product Slider
Plugin URI: http://getshopped.org
Description: A plugin that provides a slider for product list
Author: Instinct
Author URI: http://getshopped.org
Developer: sasky
Developer URI:  http://sasky.org
Version: 1.0
License: Commericial
*/

// This option is the only option that the product slider uses. It is deleted when the WPEC Product slider
// is deactivated. So if the option does not exist then the plugin is not installed.
if(get_option( 'wpec_product_slider' )){
    class Wpsc_ps_gs_admin  {
        const SETTINGS_SECTION_GS    = 'product_slider_gs';
        private $option = array();

        public function __construct(){
            add_action('wpsc-ps-admin-page-form', array( &$this, 'admin_form'));
            add_filter('wpsc-ps-default-option',  array( &$this, 'set_default_options'));
            add_filter('wpsc_ps_add_theme_to_menu', array( &$this, 'add_to_menu'));

            $option = get_option( 'wpec_product_slider' );
            // set defaults if not set

            if(!array_key_exists('gs_options',$this->option)){
                $option = $this->set_default_options( $option );
                update_option('wpec_product_slider', $option );
            }

        }
        public function set_default_options( $defaults ){
            $defaults['gs_options'] = array(  'image-width' => 500,
                                              'image-height'=> 400 );


            return $defaults;
        }
        public function add_to_menu( $theme_list ){
            $this->option = get_option( 'wpec_product_slider' );

            $theme_list[] = array( 'value' => 'Wpsc_ps_gs_theme'   ,  'title' => __('Google Splash','wpsc'));
            return $theme_list;
        }

        public function admin_form(){


            add_settings_section(   self::SETTINGS_SECTION_GS,
                                      __( '', 'wpsc' ),
                                      array( &$this, 'gs_html'),
                                        Wpsc_ps_admin_page::SETTINGS_PAGE);

              add_settings_field( 'gs_field_image_width',__( 'Image Width', 'wpsc'), array( &$this, 'gs_field_image_width'), Wpsc_ps_admin_page::SETTINGS_PAGE, self::SETTINGS_SECTION_GS);
              add_settings_field( 'gs_field_image_height',__( 'Image Height', 'wpsc'), array( &$this, 'gs_field_image_height'), Wpsc_ps_admin_page::SETTINGS_PAGE, self::SETTINGS_SECTION_GS);
        }
        public function gs_html(){
            echo '<div class="misc-pub-section gs"></div>';
            echo '<h4 class="gs">Google Splash Theme Settings</h4>';
        }
        public function gs_field_image_width(){
            WPSC_Form_helper::number_input(WPSC_PS_MAIN_OPTION . '[gs_options][image-width]',$this->option['gs_options']['image-width'],3);
            echo '<p class="install-help" >The width of the images in pixels. Enter a value between 50 and 800</p>';
        }
        public function gs_field_image_height(){
            WPSC_Form_helper::number_input(WPSC_PS_MAIN_OPTION . '[gs_options][image-height]',$this->option['gs_options']['image-height'],3);
            echo '<p class="install-help" >The height of the images in pixels. Enter a value between 50 and 800</p>';
        }
        public function admin_save(){

        }
    }
    new Wpsc_ps_gs_admin();


    Class Wpsc_ps_gs_theme {

        public function load_css(){
            wp_enqueue_style('ps-wordpress', plugins_url('css/wpsc-ps-gs.css', __FILE__ ));
        }
        public function load_scripts(){
            wp_enqueue_script('jquery-ui-slider', plugins_url('/js/wpsc-ps-gs.js' , __FILE__ ) , array('jquery', 'livequery'));
        }
        public function get_theme_data( ){
            $theme_data = array( 'theme'=> '', 'width' => 0, 'height' => 0);
            $master_option = get_option(WPSC_PS_MAIN_OPTION);
            $theme_data['theme'] = 'gs';
            $theme_data['width'] = $master_option['gs_options']['image-width'];
            $theme_data['height'] = $master_option['gs_options']['image-height'];

            return $theme_data;
        }
        public function render( $product_slider_data ){
            Wpsc_ps_Unquie_id::new_id();
            $id = Wpsc_ps_Unquie_id::get_id();
            $main_option = get_option(WPSC_PS_MAIN_OPTION);?>
            <div id="wpsc-ps-sliderWrap" >
                <div id="psc-ps-sidebar" >
                <?php foreach( $product_slider_data as $po ): ?>
                <?php endforeach ?>
                </div>
                <div id="psc-ps-container" >
                <?php foreach( $product_slider_data as $po ): ?>
                <?php endforeach ?>
                </div>
            </div>
            <?php
        }

    }
} else {
    add_action('admin_notices', 'wpec_ps_gs_show_error_message');
}
function wpec_ps_gs_show_error_message(){
    $msg = '<div id="message" class="error"xmlns="http://www.w3.org/1999/html">
            <strong>Sorry this plugin requires that you also have the WPEC Product slider already installed and activated. You can grab a copy<a href="http://getshopped.org/premium-upgrades/premium-plugin/product-slider-2010/"  target="_blank"> here </a></strong>
            </div>';
    echo $msg;
}